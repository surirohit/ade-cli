.. _addargs:

Custom ``docker run`` arguments
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ade`` encapsulates most of the options needed to start and run the Docker container
with the correct ``docker run`` options. However, a power user of Docker may find
certain options missing. ``ade start`` allows users to pass additional arguments to
``docker run``.

To pass options, separate them with ``--`` from the preceding ``ade`` options.
For example, use the following command to mount port 1234 into ``ade``::

   ade start -- --publish 127.0.0.1:1234:1234/udp

See https://docs.docker.com/engine/reference/commandline/run/ for more information on ``docker run``.

See :ref:`start-ade-macvlan` and :ref:`mounting-usb` for other examples of ``ADDARGS``.

See :ref:`aderc-file` for ways to make these options permanent for a project.


Starting ADE with ``--net=host``
""""""""""""""""""""""""""""""""

The Docker option ``--net=host`` configures the Docker container (and by extension the ADE instance) to share
its network stack with the host. This option is useful when trying to debug networking applications; however, the
option has been known to cause issues with certain UI applications such as ``glxgears`` and ``rviz``. To get
the UI applications running, use ``--privileged`` in addition to ``--net=host``: e.g

.. code:: bash

   ade start -- --net=host --privileged


**Warning**: Using ``--privileged`` is dangerous since it gives Docker nearly all
the same access to the host as processes running outside containers on the host
(see the `Docker documentation`_). If it is necessary for the ADE instance to appear
as a device on the host's network, try starting ADE with Macvlan instead
(see :ref:`start-ade-macvlan`).

.. _Docker documentation: https://docs.docker.com/engine/reference/run/#runtime-privilege-and-linux-capabilities
